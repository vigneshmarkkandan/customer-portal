import React, { Component } from "react";
import CustomerEntry from "./customerEntry.jsx";

export default class CustomerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customers: ""
    };
  }

  render() {
    let customers = this.props.customerList;
    const trItem = customers.map((item, index) => (
      <CustomerEntry
        key={index}
        customer={item}
        index={index}
        updateCustomer={this.props.updateCustomer}
        deleteCustomer={this.props.deleteCustomer}
      />
    ));
    return <tbody>{trItem}</tbody>;
  }
}
