import React, { Component } from "react";

export default class CustomerEntry extends Component {
  constructor(props) {
    super(props);
    this.state = { isEdit: false };
    this.editCustomer = this.editCustomer.bind(this);
    this.updateCustomer = this.updateCustomer.bind(this);
  }
  deleteCustomer = () => {
    const { id } = this.props.customer;
    this.props.deleteCustomer(id);
  };
  editCustomer() {
    this.setState((prevState, props) => ({
      isEdit: !prevState.isEdit
    }));
  }
  updateCustomer() {
    this.setState((prevState, props) => ({
      isEdit: !prevState.isEdit
    }));
    this.props.updateCustomer(
      this.props.customer.id,
      this.firstnameInput.value,
      this.lastnameInput.value,
      this.dobInput.value
    );
  }
  render() {
    const { firstname, lastname, dob } = this.props.customer;
    return this.state.isEdit === true ? (
      <tr className="bg-warning" key={this.props.index}>
        <td>
          <input
            ref={firstnameInput => (this.firstnameInput = firstnameInput)}
            defaultValue={firstname}
          />
        </td>
        <td>
          <input
            defaultValue={lastname}
            ref={lastnameInput => (this.lastnameInput = lastnameInput)}
          />
        </td>
        <td>
          <input
            ref={dobInput => (this.dobInput = dobInput)}
            defaultValue={dob}
          />
        </td>
        <td>
          <i className="far fa-save" onClick={this.updateCustomer}></i>
        </td>
        <td>
          <i className="fas fa-trash"></i>
        </td>
      </tr>
    ) : (
      <tr key={this.props.index}>
        <td>{firstname}</td>
        <td>{lastname}</td>
        <td>{dob}</td>
        <td>
          <i className="far fa-edit" onClick={this.editCustomer}></i>
        </td>
        <td>
          <i className="fas fa-trash" onClick={this.deleteCustomer}></i>
        </td>
      </tr>
    );
  }
}
