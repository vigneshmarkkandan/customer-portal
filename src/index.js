import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { createStore } from "redux";
import customerReducer from "./reducers/customerReducer";
import { Provider } from "react-redux";

let initialState = [
  { id: 1, firstname: "Vignesh", lastname: "Markkandan", dob: "08 Oct 1990" },
  { id: 2, firstname: "Jane", lastname: "Doe", dob: "08 Jan 1978" },
  { id: 3, firstname: "Terry", lastname: "Adams", dob: "29 Dec 1990" },
  { id: 4, firstname: "Sarah", lastname: "Patterson", dob: "29 Dec 1990" }
];
localStorage.setItem("customers", JSON.stringify(initialState));
localStorage.setItem("org_customers", JSON.stringify(initialState));

const store = createStore(customerReducer, initialState);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
