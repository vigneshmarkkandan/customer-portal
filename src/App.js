import React, { Component } from "react";
import "./App.css";
import CustomerList from "./components/customerList.jsx";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Modal from "react-awesome-modal";
import {
  addCustomer,
  deleteCustomer,
  editCustomer,
  searchCustomer
} from "./actions/customerActions";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      firstnameValue: "",
      lastnameValue: "",
      dobValue: ""
    };
  }

  openModal() {
    this.setState({
      visible: true
    });
  }

  closeModal() {
    this.setState({
      visible: false
    });
  }
  addNewCustomer = () => {
    this.props.addCustomer({
      id:
        Math.max(
          ...this.props.customerList.map(function(o) {
            return o.id;
          })
        ) + 1,
      firstname: this.state.firstnameValue,
      lastname: this.state.lastnameValue,
      dob: this.state.dobValue
    });
  };

  deleteCustomer = id => {
    let r = window.confirm("Do you want to delete this item");
    if (r === true) {
      this.props.deleteCustomer(id);
    }
  };
  updateCustomer = (id, firstname, lastname, dob) => {
    this.props.editCustomer({
      id: id,
      firstname: firstname,
      lastname: lastname,
      dob: dob
    });
  };

  handleFormChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.closeModal();
    this.addNewCustomer();
  };

  handleSearch = event => {
    let filterData = localStorage.getItem("org_customers");
    let parsedData = JSON.parse(filterData);
    let input = event.target.value;
    const query = input ? input.toLowerCase() : "";
    if (parsedData && filterData) {
      const filteredUsers = parsedData.filter(item => {
        return (
          item.firstname.toLowerCase().indexOf(query) >= 0 ||
          item.lastname.toLowerCase().indexOf(query) >= 0
        );
      });
      this.searchCustomer(filteredUsers);
    }
  };

  searchCustomer = customer => {
    this.props.searchCustomer(customer);
  };

  render() {
    const { visible, firstnameValue, lastnameValue, dobValue } = this.state;
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col-lg-12">
            <div className="card">
              <div className="card-header">Customer Portal</div>
              <div className="card-body">
                <div className="form-group">
                  <input
                    className="form-control"
                    type="text"
                    placeholder="Search Customer"
                    aria-label="Search"
                    onChange={this.handleSearch}
                  ></input>
                </div>
                <table className="table table-hover">
                  <thead className="thead-dark">
                    <tr>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>DOB</th>
                      <th>Edit/Save</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <CustomerList
                    deleteCustomer={this.deleteCustomer}
                    customerList={this.props.customerList}
                    updateCustomer={this.updateCustomer}
                  />
                </table>
                <button
                  className="btn btn-dark pull-left"
                  onClick={() => this.openModal()}
                >
                  Add New Customer
                </button>
                <Modal
                  visible={visible}
                  width="400"
                  height="300"
                  effect="fadeInUp"
                  onClickAway={() => this.closeModal()}
                >
                  <form className="form-custom" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                      <input
                        type="text"
                        name="firstnameValue"
                        className="form-control input-custom"
                        placeholder="First Name"
                        value={firstnameValue}
                        onChange={this.handleFormChange}
                      />{" "}
                    </div>
                    <div className="form-group">
                      <input
                        name="lastnameValue"
                        type="text"
                        value={lastnameValue}
                        onChange={this.handleFormChange}
                        className="form-control input-custom"
                        placeholder="Last Name"
                      />
                    </div>
                    <div className="form-group">
                      <input
                        name="dobValue"
                        type="text"
                        value={dobValue}
                        onChange={this.handleFormChange}
                        className="form-control input-custom"
                        placeholder="DOB"
                      />{" "}
                    </div>
                    <button type="submit" className="btn btn-primary">
                      Submit
                    </button>
                  </form>
                </Modal>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    customerList: state
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      addCustomer: addCustomer,
      deleteCustomer: deleteCustomer,
      editCustomer: editCustomer,
      searchCustomer: searchCustomer
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
