export function addCustomer(customer) {
  return {
    type: "ADD_CUSTOMER",
    payload: customer
  };
}

export function deleteCustomer(Id) {
  return {
    type: "DELETE_CUSTOMER",
    payload: Id
  };
}

export function editCustomer(customer) {
  return {
    type: "EDIT_CUSTOMER",
    payload: customer
  };
}

export function searchCustomer(customer) {
  return {
    type: "SEARCH_CUSTOMER",
    payload: customer
  };
}
