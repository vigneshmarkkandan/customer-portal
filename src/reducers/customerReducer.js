const customerReducer = (state = [], action) => {
  switch (action.type) {
    case "ADD_CUSTOMER":
      let stateCopy = [...state, action.payload];
      localStorage.setItem("customers", JSON.stringify(stateCopy));
      localStorage.setItem("org_customers", JSON.stringify(stateCopy));
      return stateCopy;

    case "DELETE_CUSTOMER":
      stateCopy = state.filter(x => x.id !== action.payload);
      localStorage.setItem("customers", JSON.stringify(stateCopy));
      localStorage.setItem("org_customers", JSON.stringify(stateCopy));
      return stateCopy;

    case "EDIT_CUSTOMER":
      stateCopy = state.map(customer => {
        const { id, firstname, lastname, dob } = action.payload;
        if (customer.id === id) {
          customer.firstname = firstname;
          customer.lastname = lastname;
          customer.dob = dob;
        }
        return customer;
      });
      localStorage.setItem("customers", JSON.stringify(stateCopy));
      localStorage.setItem("org_customers", JSON.stringify(stateCopy));
      return stateCopy;

    case "SEARCH_CUSTOMER":
      stateCopy = [action.payload];
      localStorage.setItem("customers", JSON.stringify(action.payload));
      return action.payload;

    default:
      return state;
  }
};
export default customerReducer;
